# Dokumentový modul

Toto je dočasná implementace dokumentového úložiště. Později se rozdělí a rozkopíruje se na jednotlivé projekty (common, services..).
Aktuálně obsahuje:

* Jádro dokumentového modulu
* API pro přístup k dokumentům - čtení, zápis
* API pro náhledy k dokumentům
* Extractor - extrakce metadat z uložených dokumentů. Extrakce obsahu PDF souborů.
* Uploader - kontinuální migrace z daného adresáře
* API pro získání PDF a ZIP z komunikační historie - bude přesunuto to backendu CORE.
* Pumpa pro přenos vybraných souborů z PROD do DEV.

## How to build
`mvn package`

## How to run
Create `application.properties` and insert correct password for mongo and postgresql database.

```properties
#Enable API
api.enabled=true

#Enable metadata extractor for PDF files
#extractor.enabled=true
#extractor.fullFileNameRegEx=pdf$
#extractor.threads=2

#Connection to the devel database
mongodb.uri=mongodb://storageUser:XXX@app-doc-dev-01,app-doc-dev-02,app-doc-dev-03/storage
mongodb.databaseName=storage

#Connection to data database 
spring.datasource.url= jdbc:postgresql://sql-dev-01:5432/insurance_core
spring.datasource.username=adinsure_importer
spring.datasource.password=XXX


#uploader.enabled=false
#uploader.dryRun=false
#uploader.uuidPumpEnabled=true
#uploader.imports[0].name=5p
#uploader.imports[0].source=upl_5p
#uploader.imports[0].importBaseFolder=/home/roman/direct/pdf
#uploader.imports[0].destPrefixFolder=/var/documents/policyDocuments10
#uploader.imports[0].waitingTime=5s
#uploader.imports[0].recursive=true

#Copy some files from PROD to our environment
#Developer mongo database does not contain all files from production. Here the selection PROD->DEV is defined.
#copy.sourceAPI=http://app-doc-prod-01:8080
#copy.sql=SELECT file_path_tmp \
#FROM comm.communication \
#inner join comm.communication_attachment on comm.communication_attachment.communication_id=comm.communication.id \
#where sender_contact_value='petr.hanzelka77@gmail.com' or recipient_contact_value='petr.hanzelka77@gmail.com'

``` 
The JAR file is executable. Simply run:
`./target/storage-0.0.1-SNAPSHOT.jar`

