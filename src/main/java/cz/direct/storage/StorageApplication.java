package cz.direct.storage;

import cz.direct.storage.copy.CopyService;
import cz.direct.storage.extractor.ExtractorService;
import cz.direct.storage.uploader.UploaderService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.io.FileInputStream;
import java.util.Arrays;

@SpringBootApplication
public class StorageApplication {

	public static void main(String[] args) {

		SpringApplication.run(StorageApplication.class, args);


	}


	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			ExtractorService extractorService = (ExtractorService)ctx.getBean("extractorService");
			extractorService.startExtractor();

			UploaderService uploaderService = (UploaderService)ctx.getBean("uploaderService");
			uploaderService.start();

			//one time run if it is specified in properties file
			CopyService copyService = (CopyService)ctx.getBean("copyService");
			copyService.copy();
		};
	}

}
