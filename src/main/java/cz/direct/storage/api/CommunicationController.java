package cz.direct.storage.api;

import cz.direct.storage.communication.CommunicationService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Controller
public class CommunicationController {

    @Autowired
    CommunicationService communicationService;

    @GetMapping("/communication/convertToPDF/{uuid:.+}")
    @ApiOperation(value = "Returns communication as one PDF file")
    public void getCommunicationAsPDF(@PathVariable @ApiParam(value = "Unique ID of communication") String uuid, @RequestParam(required = false) boolean excludeContent, @RequestParam(required = false) boolean excludeAttachment,HttpServletResponse response) throws Exception {
        response.setHeader("Content-Type", "application/pdf");
        setFileName("komunikace.pdf",response);
        OutputStream os = response.getOutputStream();
        communicationService.communicationAsPDF(os,Arrays.asList(UUID.fromString(uuid)),excludeContent,excludeAttachment);
        os.close();
    }

    @GetMapping("/communication/convertToZIP/{uuid:.+}")
    @ApiOperation(value = "Returns communication as one ZIP file")
    public void getCommunicationAsZIP(@PathVariable @ApiParam(value = "Unique ID of communication") String uuid, @RequestParam(required = false) boolean excludeContent, @RequestParam(required = false) boolean excludeAttachment, HttpServletResponse response) throws Exception {
        response.setHeader("Content-Type", "application/zip");
        setFileName("komunikace.zip",response);
        OutputStream os = response.getOutputStream();
        communicationService.communicationAsZIP(os,Arrays.asList(UUID.fromString(uuid)),excludeContent,excludeAttachment);
        os.close();
    }

    @GetMapping("/communication/daktelaConvertToPDF/{daktelaId:.+}")
    @ApiOperation(value = "Returns daktela ticket as one PDF file")
    public void getCommunicationAsPDF(@PathVariable @ApiParam(value = "Id of daktela ticket") long daktelaId, @RequestParam(required = false) boolean excludeContent, @RequestParam(required = false) boolean excludeAttachment,HttpServletResponse response) throws Exception {
        response.setHeader("Content-Type", "application/pdf");
        setFileName("komunikace-daktela.pdf",response);
        OutputStream os = response.getOutputStream();
        List<UUID> uuids = communicationService.daktelaTicketToUUIDs(daktelaId);
        communicationService.communicationAsPDF(os,uuids,excludeContent,excludeAttachment);
        os.close();
    }

    @GetMapping("/communication/daktelaConvertToZIP/{daktelaId:.+}")
    @ApiOperation(value = "Returns daktela ticket as one ZIP file")
    public void getCommunicationAsZIP(@PathVariable @ApiParam(value = "Id of daktela ticket") long daktelaId, @RequestParam(required = false) boolean excludeContent, @RequestParam(required = false) boolean excludeAttachment, HttpServletResponse response) throws Exception {
        response.setHeader("Content-Type", "application/zip");
        setFileName("komunikace-daktela.zip",response);
        OutputStream os = response.getOutputStream();
        List<UUID> uuids = communicationService.daktelaTicketToUUIDs(daktelaId);
        communicationService.communicationAsZIP(os,uuids,excludeContent,excludeAttachment);
        os.close();
    }

    private static void setFileName(String name,HttpServletResponse response) {
        try {
            String fileName = URLEncoder.encode(name, "UTF-8");
            response.setHeader("Content-Disposition","filename*=UTF-8''"+fileName);
        } catch (UnsupportedEncodingException e) {
            //never happens
           throw new RuntimeException(e);
        }
    }

}
