package cz.direct.storage.api.access;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;

@Component
public class AccessRightInterceptor extends HandlerInterceptorAdapter {
    private Logger logger = Logger.getLogger(AccessRightInterceptor.class.getName());

    @Autowired
    ApiConfigurationProperties apiConfigurationProperties;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {

        String path = request.getServletPath();
        if(path.equals("/")) {
            return super.preHandle(request, response, handler);
        }

        if(!apiConfigurationProperties.isEnabled()) {
            response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            return false;
        }

        if(path.startsWith("/get")) {
            //TODO temporal access, remove it after May 2018
            return super.preHandle(request, response, handler);
        }

        //swagger access
        if(path.startsWith("/swagger") || path.startsWith("/webjars") || path.startsWith("/v2/api-docs")) {
            return super.preHandle(request, response, handler);
        }

        if(apiConfigurationProperties.getIpAllowed().size() > 0) {
            String ipAddress = request.getRemoteAddr();
            if(!apiConfigurationProperties.getIpAllowed().contains(ipAddress)) {
                logger.info("Acces rejected for: " + ipAddress);
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                return false;
            }
        }

        return super.preHandle(request, response, handler);
    }
}
