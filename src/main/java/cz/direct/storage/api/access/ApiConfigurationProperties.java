package cz.direct.storage.api.access;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties("api")
public class ApiConfigurationProperties {
    private boolean enabled = false;
    private List<String> ipAllowed = new ArrayList<>();

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<String> getIpAllowed() {
        return ipAllowed;
    }

    public void setIpAllowed(List<String> ipAllowed) {
        this.ipAllowed = ipAllowed;
    }
}
