package cz.direct.storage.api.doc;

import cz.direct.storage.extractor.MimeTypeService;
import cz.direct.storage.storage.FileInfo;
import cz.direct.storage.storage.Storage;
import cz.direct.storage.preview.PreviewInfo;
import cz.direct.storage.preview.PreviewService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class DocController {

    Logger logger = Logger.getLogger(DocController.class.getName());

    @Autowired
    Storage storage;

    @Autowired
    PreviewService previewService;

    @Autowired
    MimeTypeService mimeTypeService;

    @GetMapping("/")
    @ResponseBody
    public String welcome() {
        return "Storage is running v2";
    }

    @GetMapping("/get/{uuid:.+}")
    @ApiOperation(value = "Returns a file based on given UUID")
    public void getFile(@PathVariable String uuid, HttpServletResponse response) throws IOException {
        try (InputStream fileStream = storage.getFile(uuid)) {
            if (fileStream == null) {
                response.sendError(HttpStatus.NOT_FOUND.value());
                logger.info("Not found: " + uuid);
                return;
            }

            response.setCharacterEncoding("UTF-8");

            FileInfo fileInfo = storage.getFileInfo(uuid);
            String contentType = mimeTypeService.getMimeType(fileInfo.getFullFileName());
            if(contentType == null) {
                //not analyzed, guessing
                if(fileInfo.getFullFileName().toLowerCase().endsWith(".pdf")) contentType = "application/pdf";
                if(fileInfo.getFullFileName().toLowerCase().endsWith(".jpg")) contentType = "image/jpeg";
            }

            if(contentType != null) {
                response.setContentType(contentType);
            }

            response.setContentLengthLong(fileInfo.getLength());

            String fileName = URLEncoder.encode(new File(fileInfo.getFullFileName()).getName(), "UTF-8");
            response.setHeader( "Content-Disposition", "filename*=UTF-8''" + fileName);

            OutputStream clientStream = response.getOutputStream();
            IOUtils.copy(fileStream, clientStream);
            clientStream.close();

            if(logger.isLoggable(Level.FINE)) {
                logger.log(Level.INFO, "Success get: " + uuid);
            }
        }
    }

    @GetMapping("/preview/{uuid:.+}")
    @ApiOperation(value = "Returns a preview based on given UUID")
    public void getPreviewFile(@PathVariable String uuid, @RequestParam(value="width",required = false) Integer width, @RequestParam(value="height",required = false) Integer height, HttpServletResponse response) throws IOException {
        PreviewInfo tStream = previewService.getThumbnail(uuid,width,height);
        response.setHeader("Content-Length",Long.toString(tStream.getFileLength()));
        response.setHeader("Content-Type",tStream.getMimeType());
        OutputStream cStream = response.getOutputStream();
        IOUtils.copy(new ByteArrayInputStream(tStream.getImage()), cStream);
        cStream.close();

        if(logger.isLoggable(Level.FINE)) {
            logger.log(Level.INFO, "Success preview: " + uuid);
        }

    }
    @GetMapping("/fileInfo")
    @ApiOperation(value = "Returns information about the file")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved info"),
            @ApiResponse(code = 400, message = "Missing UUID or fullFileName"),
            @ApiResponse(code = 404, message = "File is not found"),
            @ApiResponse(code = 501, message = "fullFileName is not unique")
    })
    public ResponseEntity<FileInfoResult> getFileInfo(@RequestParam(value="uuid",required = false) String uuid,@RequestParam(value="fullFileName",required = false) String fullFileName) {
        if(uuid != null) {
            FileInfo fi = storage.getFileInfo(uuid);
            if(fi == null)  {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(FileInfoResult.createFromFileInfo(fi),HttpStatus.OK);
        }
        if(fullFileName != null) {
            List<FileInfo> listFileInfo = storage.getFileInfoByFullFileName(fullFileName);
            if(listFileInfo.size() == 0) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            if(listFileInfo.size() > 1) {
                return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
            }
            return new ResponseEntity<>(FileInfoResult.createFromFileInfo(listFileInfo.get(0)),HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/put", method = RequestMethod.POST)
    public ResponseEntity<?>  putFile(
                              @RequestParam String uuid,
                              @RequestParam String source,
                              @RequestParam String fileName,
                              @RequestParam(required = true) MultipartFile file) throws IOException {

        try (InputStream clientStream = file.getInputStream()) {
            try {
                storage.storeFile(clientStream, uuid, source, fileName,null);
            } catch(Exception e) {
                logger.log(Level.WARNING,"Cannot put file " + uuid + " " + e.getMessage());
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        }

        if(logger.isLoggable(Level.FINE)) {
            logger.log(Level.INFO, "Success put: " + uuid);
        }
        return new ResponseEntity("Success",HttpStatus.OK);
    }
}
