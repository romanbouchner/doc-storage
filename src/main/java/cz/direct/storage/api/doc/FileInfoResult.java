package cz.direct.storage.api.doc;

import cz.direct.storage.storage.FileInfo;

public class FileInfoResult {
    private String uuid;
    private long length;
    private String fullFileName;

    public FileInfoResult() {

    }

    public FileInfoResult(String uuid, long length, String fullFileName) {
        this.uuid = uuid;
        this.length = length;
        this.fullFileName = fullFileName;
    }

    public static FileInfoResult createFromFileInfo(FileInfo fileInfo) {
        return new FileInfoResult(fileInfo.getUuid(),fileInfo.getLength(),fileInfo.getFullFileName());
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public String getFullFileName() {
        return fullFileName;
    }

    public void setFullFileName(String fullFileName) {
        this.fullFileName = fullFileName;
    }
}
