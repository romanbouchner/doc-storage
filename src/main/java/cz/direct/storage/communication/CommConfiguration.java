package cz.direct.storage.communication;


import cz.direct.services.template.component.interfaces.PDFCreatorComponent;
import cz.direct.storage.communication.pdf.PDFCreatorComponentExternalImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommConfiguration {

    @Bean
    PDFCreatorComponent pdfCreatorComponent() {
        //our implementation has no font dependency
        return new PDFCreatorComponentExternalImpl();
    }

}
