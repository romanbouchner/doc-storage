package cz.direct.storage.communication;

import java.util.UUID;

public class CommunicationAttachment {
    private final long id;
    private final String displayName;
    private final String contentType;
    private final UUID fileUuid;

    public CommunicationAttachment(long id, String displayName, String contentType, UUID fileUuid) {
        this.id = id;
        this.displayName = displayName;
        this.contentType = contentType;
        this.fileUuid = fileUuid;
    }

    /**
     *
     * @return unique ID of attachment
     */
    public long getId() {
        return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getContentType() {
        return contentType;
    }

    /**
     * @return reference to the file in document module.
     */
    public UUID getFileUuid() {
        return fileUuid;
    }

    @Override
    public String toString() {
        return "CommunicationAttachment{" +
                "id=" + id +
                ", displayName='" + displayName + '\'' +
                ", contentType='" + contentType + '\'' +
                ", fileUuid=" + fileUuid +
                '}';
    }
}
