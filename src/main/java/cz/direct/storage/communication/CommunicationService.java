package cz.direct.storage.communication;

import cz.direct.common.exception.BusinessException;
import cz.direct.services.template.component.interfaces.PDFCreatorComponent;
import cz.direct.storage.communication.dao.CommunicationDetail;
import cz.direct.storage.communication.dao.CommunicationRepository;
import cz.direct.storage.storage.Storage;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

//select * from comm.communication where daktela_ticket_id=426248

@Service
public class CommunicationService {

    @Autowired
    private CommunicationRepository repository;

    @Autowired
    Storage documentStorage;

    @Autowired
    private PDFCreatorComponent pdfCreatorComponent;

    public void communicationAsPDF(OutputStream os, List<UUID> communicationIds, boolean excludeContent, boolean excludeAttachments) throws BusinessException {
        ArrayList<File> files = new ArrayList<>();

        try {
            for (UUID communicationId : communicationIds) {
                if (!excludeContent) {
                    CommunicationDetail commDetail = repository.getCommunicationDetail(communicationId);
                    if (commDetail!=null && commDetail.getContent() != null) {
                        byte[] bytes = pdfCreatorComponent.printHtml(fixHeaderEncoding(commDetail.getContent()));

                        File tmpFile = File.createTempFile("commservice", ".pdf");
                        files.add(tmpFile);
                        try (FileOutputStream fos = new FileOutputStream(tmpFile)) {
                            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                            IOUtils.copy(bais, fos);
                        }
                    }
                }

                if (!excludeAttachments) {
                    List<CommunicationAttachment> attachments = repository.getCommunicationAttachments(communicationId);
                    for (CommunicationAttachment attachment : attachments) {
                        if (!attachment.getContentType().equalsIgnoreCase("application/pdf")) {
                            throw new CommunicationUnexpectedException("Unsupported content: " + attachment.getContentType(), null);
                        }

                        if (attachment.getFileUuid() == null) {
                            continue;
                            //TODO it should never happen when migration is complete, add exception later
                            //throw new CommunicationUnexpectedException("Missing file record on communication " + communicationId.toString(),null);
                        }

                        try (InputStream dis = documentStorage.getFile(attachment.getFileUuid().toString())) {
                            if (dis == null) {
                                continue;
                                //TODO it should never happen when migration is complete, add exception later
                                //throw new CommunicationUnexpectedException("File " + attachment.getFileUuid().toString() + " does not exist on file storage",null);
                            }

                            File tmpFile = File.createTempFile("commservice", ".pdf");
                            files.add(tmpFile);
                            try (FileOutputStream fos = new FileOutputStream(tmpFile)) {
                                IOUtils.copy(dis, fos);
                            }
                        }
                    }
                }
            }
            byte[] result = pdfCreatorComponent.merge(files);
            ByteArrayInputStream bais = new ByteArrayInputStream(result);
            IOUtils.copy(bais, os);
            os.close();

        } catch (IOException e) {
            throw new CommunicationUnexpectedException("Cannot create PDF file from communication", e);
        } finally {
            //always clean temporary files
            files.forEach(File::delete);
        }
    }

    public void communicationAsZIP(OutputStream outputStream, List<UUID> communicationIds, boolean excludeContent, boolean excludeAttachments) throws BusinessException {
        FileNameGenerator fn = new FileNameGenerator();
        try {
            ZipOutputStream zos = new ZipOutputStream(outputStream);

            for(UUID communicationId: communicationIds) {

                    CommunicationDetail commDetail = repository.getCommunicationDetail(communicationId);
                    String mainFileName = fn.generateName(commDetail!=null?commDetail.getSubject():"",60);
                    if (!excludeContent && commDetail != null && commDetail.getContent() != null) {
                        byte[] bytes = pdfCreatorComponent.printHtml(fixHeaderEncoding(commDetail.getContent()));
                        ZipEntry entry = new ZipEntry(mainFileName + ".pdf");
                        zos.putNextEntry(entry);
                        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                        IOUtils.copy(bais, zos);
                        zos.closeEntry();
                    }



                if (!excludeAttachments) {
                    List<CommunicationAttachment> attachments = repository.getCommunicationAttachments(communicationId);
                    for (CommunicationAttachment attachment : attachments) {
                        if (attachment.getFileUuid() == null) {
                            continue;
                            //throw new CommunicationUnexpectedException("Missing file record on communication " + communicationId.toString(),null);
                        }
                        try (InputStream dis = documentStorage.getFile(attachment.getFileUuid().toString())) {
                            if (dis == null) {
                                continue;
                                //throw new CommunicationUnexpectedException("File " + attachment.getFileUuid().toString() + " does not exist on file storage",null);
                            }
                            ZipEntry entry = new ZipEntry(mainFileName+ "/" + attachment.getDisplayName());
                            zos.putNextEntry(entry);
                            IOUtils.copy(dis, zos);
                            zos.closeEntry();
                        }
                    }
                }
            }

            zos.close();
        } catch (IOException e) {
            throw new CommunicationUnexpectedException("Cannot create ZIP file from communication", e);
        }
    }

    public List<UUID> daktelaTicketToUUIDs(long daktelaId) {
        return repository.getCommunicationDaktelaDetail(daktelaId).stream().map(cdetail -> cdetail.getUuid()).collect(Collectors.toList());
    }

    private String fixHeaderEncoding(String html) {
        String lcHtml = html.toLowerCase();
        if(!lcHtml.contains("<html>") && !lcHtml.contains("<body>")) {
            return "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>" + html + "</body></html>";
        }
        return html;
    }
}
