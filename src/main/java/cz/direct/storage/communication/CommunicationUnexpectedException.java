package cz.direct.storage.communication;


import cz.direct.common.exception.BusinessException;

public class CommunicationUnexpectedException extends BusinessException {
    public CommunicationUnexpectedException(String message,Throwable t) {
        super(message,t);
    }

    @Override
    public Exception getException() {
        return Exception.UNEXPECTED;
    }
}
