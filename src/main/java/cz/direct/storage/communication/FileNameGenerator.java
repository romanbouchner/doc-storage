package cz.direct.storage.communication;

import java.util.HashMap;

/**
 * Generates unique filename from tile
 */
public class FileNameGenerator {
    private HashMap<String,String> usedNames = new HashMap<>();

    public String generateName(String title, int maxLen) {
        String sanitized = sanitizeFileName(title,maxLen);
        String generated = sanitized;
        int cnt = 1;
        while(usedNames.get(generated.toLowerCase()) != null) {
            generated = Integer.toString(cnt++) + "-" + sanitized;
        }
        usedNames.put(generated.toLowerCase(),generated);
        return generated;
    }

    private static String sanitizeFileName(String name, int maxLen) {
        if(name == null || name.trim().isEmpty()) return "NA";

        StringBuffer res = new StringBuffer();
        for(int i=0; i<name.length() && i<maxLen;i++) {
            char ch = name.charAt(i);
            if(Character.isWhitespace(ch)) {
                res.append("_");
                continue;
            }
            if(Character.isAlphabetic(ch) || Character.isDigit(ch)) {
                res.append(ch);
            }
            if(ch == '-') {
                res.append(ch);
            }
        }
        return res.toString();
    }
}
