package cz.direct.storage.communication.dao;

import java.util.UUID;

public class CommunicationDetail {
    private final UUID uuid;
    private final String subject;
    private final String content;

    public CommunicationDetail(UUID uuid, String subject, String content) {
        this.uuid = uuid;
        this.subject = subject;
        this.content = content;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }
}
