package cz.direct.storage.communication.dao;

import cz.direct.storage.communication.CommunicationAttachment;

import java.util.List;
import java.util.UUID;

/**
 * Access to communication database
 */
public interface CommunicationRepository {

    /**
     * Returns the content of communication history. Content is in HTML format.
     * @param communicationId unique ID of communication
     * @return
     */
    CommunicationDetail getCommunicationDetail(UUID communicationId);

    List<CommunicationDetail> getCommunicationDaktelaDetail(long daktelaId);

    /**
     * Returns all attachments from communication history.
     * @param communicationId unique ID of communication
     * @return
     */
    List<CommunicationAttachment> getCommunicationAttachments(UUID communicationId);
}
