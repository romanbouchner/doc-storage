package cz.direct.storage.communication.dao;


import cz.direct.storage.communication.CommunicationAttachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;


@Repository
public class CommunicationRepositoryImpl implements CommunicationRepository{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<CommunicationDetail> commDetailMapper = (rs, rowNumber) -> new CommunicationDetail(UUID.fromString(rs.getString("id")),rs.getString("subject"),rs.getString("body"));

    @Override
    public CommunicationDetail getCommunicationDetail(UUID communicationId) {
        String sql = "select comm.communication.id,comm.communication_content.body,comm.communication.subject from comm.communication left join comm.communication_content on comm.communication.content_id = comm.communication_content.id where comm.communication.id = ?";
        List<CommunicationDetail> result = jdbcTemplate.query(sql, new Object[]{communicationId}, commDetailMapper);
        if(result.size() == 0) return null;
        if(result.size() > 1) {
            throw new RuntimeException("Unexpected size for " + communicationId);
        }
        return result.get(0);
    }

    @Override
    public List<CommunicationDetail> getCommunicationDaktelaDetail(long daktelaId) {
        String sql = "select comm.communication.id,comm.communication_content.body,comm.communication.subject from comm.communication left join comm.communication_content on comm.communication.content_id = comm.communication_content.id where comm.communication.daktela_ticket_id = ?";
        List<CommunicationDetail> result = jdbcTemplate.query(sql, new Object[]{daktelaId}, commDetailMapper);
        return result;
    }

    @Override
    public List<CommunicationAttachment> getCommunicationAttachments(UUID communicationId) {
        String sql = "select * from comm.communication_attachment where comm.communication_attachment.communication_id=?";
        return jdbcTemplate.query(sql, new Object[]{communicationId}, (rs, rowNumber) ->
            new CommunicationAttachment(rs.getLong("id"), rs.getString("display_name"), rs.getString("content_type"), rs.getString("file_id")!=null?UUID.fromString(rs.getString("file_id")):null));
    }

}

