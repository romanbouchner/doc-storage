package cz.direct.storage.communication.pdf;

import cz.direct.common.exception.BusinessException;
import cz.direct.services.template.component.interfaces.PDFCreatorComponent;
import cz.direct.storage.communication.CommunicationUnexpectedException;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.List;
import java.util.Map;

/**
 * This class makes simple PDF manipulation with external linux commands:
 * <p>
 * wkhtmltopdf
 * pdfunite
 * <p>
 * There is no other dependency (java lib, configuration, fonts..).
 */
public class PDFCreatorComponentExternalImpl implements PDFCreatorComponent {

    private Logger logger = LogManager.getLogger(PDFCreatorComponentExternalImpl.class.getName());

    @Override
    public byte[] merge(List<File> list) {
        //prepare
        StringBuffer command = new StringBuffer("pdfunite ");
        for (File file : list) {
            command.append(file.getAbsolutePath());
            command.append(" ");
        }
        try {
            File outFile = File.createTempFile("PdfCreatorExternal", ".pdf");
            try {
                command.append(outFile.getAbsolutePath());
                //execute
                logger.debug("Executing: " + command);

                Process process = Runtime.getRuntime().exec(command.toString());
                String errOutput = processErrorStream(process);

                if (process.waitFor() == 0) {
                    return tmpFileToBytes(outFile);
                }

                //unexpected error from command line
                logger.error("Command line:" + command+ "\nOutput from command line:" + errOutput + " EXIT CODE: " + process.exitValue());
                throw new RuntimeException("Unexpected result from command line utility " + process.exitValue(), null);
            } finally {
                outFile.delete();
            }
        } catch (IOException e) {
            //interface does not allow us to raise BusinessException
            throw new RuntimeException("Cannot merge PDF files: " + e.getMessage(), e);
        } catch (InterruptedException e) {
            throw new RuntimeException("Cannot convert HTML to PDF: Interrupted " + e.getMessage(), e);
        }
    }

    @Override
    public byte[] printHtml(String str) throws BusinessException {
        try {
            //prepare input file
            File inFile = File.createTempFile("PdfCreatorExternal", ".html");
            try {
                try (PrintWriter out = new PrintWriter(inFile)) {
                    out.println(str);
                }

                //prepare output file
                File outFile = File.createTempFile("PdfCreatorExternal", ".pdf");
                try {
                    String command = "wkhtmltopdf " + inFile.getAbsolutePath() + " " + outFile.getAbsolutePath();
                    logger.debug("Executing: " + command);
                    Process process = Runtime.getRuntime().exec(command);

                    String errOutput = processErrorStream(process);

                    if (process.waitFor() == 0) {
                        return tmpFileToBytes(outFile);
                    }

                    //unexpected error from command line
                    logger.error("Output from command line:" + errOutput + " EXIT CODE: " + process.exitValue());
                    throw new CommunicationUnexpectedException("Unexpected result from command line utility " + process.exitValue(), null);
                } finally {
                    outFile.delete();
                }
            } finally {
                inFile.delete();
            }
        } catch (IOException e) {
            throw new CommunicationUnexpectedException("Cannot convert HTML to PDF: " + e.getMessage(), e);
        } catch (InterruptedException e) {
            throw new CommunicationUnexpectedException("Cannot convert HTML to PDF: Interrupted " + e.getMessage(), e);
        }

    }

    @Override
    public byte[] print(String s, Map<String, Object> map) throws BusinessException {
        throw new CommunicationUnexpectedException("Not implemented",null);
    }

    @Override
    public File overlay(File file, File file1, int i) throws BusinessException {
        throw new CommunicationUnexpectedException("Not implemented",null);
    }

    @Override
    public byte[] overlay(byte[] bytes, File file, int i) throws BusinessException {
        throw new CommunicationUnexpectedException("Not implemented",null);
    }

    private String processErrorStream(Process process) throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        String line;
        StringBuffer output = new StringBuffer();
        while ((line = input.readLine()) != null) {
            output.append(line);
            output.append("\n");
            logger.debug(line);
        }
        return output.toString();
    }

    private byte[] tmpFileToBytes(File tmpFile) throws IOException {
        //ok
        if (tmpFile.length() > 100000000) {
            //take care about server memory
            throw new RuntimeException("The PDF file is too big " + tmpFile.length(), null);
        }

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            try (FileInputStream fis = new FileInputStream(tmpFile)) {
                IOUtils.copy(fis, baos);
                return baos.toByteArray();
            }
        }
    }
}
