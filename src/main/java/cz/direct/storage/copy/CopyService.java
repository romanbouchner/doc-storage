package cz.direct.storage.copy;

import cz.direct.storage.api.doc.FileInfoResult;
import cz.direct.storage.repository.StorageRepository;
import cz.direct.storage.storage.FileInfo;
import cz.direct.storage.storage.Storage;
import cz.direct.storage.uploader.UploaderConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Copy service is designed to copy selected files from PROD environment do DEV or TEST environment.
 * The selection is based on SQL query.
 *
 * Two variant
 * 1)The SQL query must return column with file_path.
 * 2)The SQL query must return column with file uuid
 *
 *
 * If the file is not present in the storage, the file is downloaded from URI (typically PROD environment).
 * If the UUID of file is not present in migration table, the migration table is updated.
 */
@Service
public class CopyService {

    Logger logger = Logger.getLogger(CopyService.class.getName());

    @Autowired
    private Storage storage;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private CopyServiceConfigurationProperties properties;

    @Autowired
    StorageRepository storageRepository;

    public void copy() {
        copyWithFilePath();
        copyWithFileUUID();
    }

    private void copyWithFilePath() {
        if(properties.getSql() == null || properties.getSourceAPI() == null) {
            return;
        }
        logger.info("== COPY SERVICE FilePath ==");
        logger.info("START Running with " + properties.getSql());
        List<String> fullFileNames = jdbcTemplate.query(properties.getSql(),(rs, rowNumber) -> rs.getString(1));
        for(String fullFileName : fullFileNames) {
            if(fullFileName != null) {
                List<FileInfo> fileInfos = storage.getFileInfoByFullFileName(fullFileName);
                if (fileInfos.size() > 1) {
                    logger.warning("File " + fullFileName + "  is not unique");
                    continue;
                }
                if (fileInfos.size() == 0) {
                    downloadAndStore("fullFileName", fullFileName);
                }
            }
        }

        logger.info("FINISH");
    }

    private void copyWithFileUUID() {
        if(properties.getSqlWithUUID() == null || properties.getSourceAPI() == null) {
            return;
        }
        logger.info("== COPY SERVICE FileUUID ==");
        logger.info("START Running with " + properties.getSqlWithUUID());
        List<String> fileUUIDs = jdbcTemplate.query(properties.getSqlWithUUID(),(rs, rowNumber) -> rs.getString(1));
        for(String fileUUID : fileUUIDs) {
            if(fileUUID != null) {
                FileInfo fileInfo = storage.getFileInfo(fileUUID);
                if (fileInfo == null) {
                    downloadAndStore("uuid", fileUUID);
                }
            }
        }

        logger.info("FINISH");
    }

    private void downloadAndStore(String paramName, String paramValue) {
        RestTemplate restTemplate = new RestTemplate();

        logger.info("File " + paramValue + " is not present. Downloading from API source");
        String url = properties.getSourceAPI() + "/fileInfo";
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam(paramName, paramValue);
        FileInfoResult fileInfo = null;

        try {
            //get file info
            logger.info("Try to get file info for " + paramValue);
            fileInfo = restTemplate.getForObject(builder.build().encode().toUri(), FileInfoResult.class);
            logger.info("Success, UUID is " + fileInfo.getUuid());
            //download file with uuid
            url = properties.getSourceAPI() + "/get/" + fileInfo.getUuid();
            byte[] file = restDownload(url);
            //upload it to our storage
            try(ByteArrayInputStream bais = new ByteArrayInputStream(file)) {
                storage.storeFile(bais,fileInfo.getUuid(),"copy",fileInfo.getFullFileName(),Arrays.asList(UploaderConst.UUID_REGISTRATION_REQUEST));
            }
            logger.info("SUCCESS " + fileInfo.getFullFileName() + " " + fileInfo.getUuid());
        } catch(RestClientException e) {
            logger.log(Level.WARNING, "Cannot get file info from " +builder.toUriString() + e.getMessage(),e);
        } catch(com.mongodb.MongoWriteException e) {
            if(e.getCode() == 11000) {
                //duplicate error, this should never happen, but it happened on dev (maybe some incorrect run on dev?). Clean the file and fix it.
                if(fileInfo != null) {
                    logger.warning("File " + fileInfo.getUuid() + " exists in gridFS, but does not exist in storageFiles. Removing the file from gridFS. Run CopyService again to finish copy process.");
                    storageRepository.deleteDataFile(fileInfo.getUuid());
                }
            }
        } catch(Exception e) {
            logger.log(Level.SEVERE, "Cannot process file file ",e);
        }
    }
    private byte[] restDownload(String url) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(
                new ByteArrayHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));

        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<byte[]> response = restTemplate.exchange(
                url,
                HttpMethod.GET, entity, byte[].class);

        return response.getBody();
    }

}
