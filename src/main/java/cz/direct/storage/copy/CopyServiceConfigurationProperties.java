package cz.direct.storage.copy;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("copy")
public class CopyServiceConfigurationProperties {
    String sql;
    String sqlWithUUID;

    String sourceAPI;

    /**
     * Where API with source document is located
     * @return
     */
    public String getSourceAPI() {
        return sourceAPI;
    }

    public void setSourceAPI(String sourceAPI) {
        this.sourceAPI = sourceAPI;
    }

    /**
     * SQL query to the local database. Must return a column with missing file_path
     * @return
     */
    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    /**
     * Same as SQL but the query must return mongo db UUID (file UUID)
     * @return
     */
    public String getSqlWithUUID() {
        return sqlWithUUID;
    }

    public void setSqlWithUUID(String sqlWithUUID) {
        this.sqlWithUUID = sqlWithUUID;
    }
}
