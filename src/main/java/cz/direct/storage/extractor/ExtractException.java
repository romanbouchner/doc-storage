package cz.direct.storage.extractor;

public class ExtractException extends Exception {
    public ExtractException(String message, Throwable t) {
        super(message,t);
    }
}
