package cz.direct.storage.extractor;

import org.apache.tika.metadata.Metadata;

public class ExtractedContent {
    private final String text;
    private final Metadata metadata;

    public ExtractedContent(String text, Metadata metadata) {
        this.text = text;
        this.metadata = metadata;
    }

    public String getText() {
        return text;
    }

    public Metadata getMetadata() {
        return metadata;
    }
}
