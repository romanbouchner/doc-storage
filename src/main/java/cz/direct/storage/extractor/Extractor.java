package cz.direct.storage.extractor;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

@Component
public class Extractor {

    /**
     * Extract metadata from file
     * @param is
     * @return
     * @throws ExtractException
     */
    public ExtractedContent extract(InputStream is) throws ExtractException {
        try {
            try {
                BodyContentHandler handler = new BodyContentHandler();
                Metadata metadata = new Metadata();
                AutoDetectParser parser = new AutoDetectParser();
                ParseContext parseContext = new ParseContext();
                parser.parse(is, handler, metadata, parseContext);
                return new ExtractedContent(handler.toString(),metadata);

            } catch (TikaException e) {
                throw new ExtractException("Tika exception " + e.getMessage(),e);
            } catch (SAXException e) {
                throw new ExtractException("SAX exception " + e.getMessage(),e);
            }
        } catch (IOException e) {
            throw new ExtractException("Cannot close stream " + e.getMessage(),e);
        }
    }

}
