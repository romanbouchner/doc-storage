package cz.direct.storage.extractor;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("extractor")
public class ExtractorConfigurationProperties {

    private boolean enabled = false;
    private String fullFileNameRegEx;
    private int threads = 2;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getFullFileNameRegEx() {
        return fullFileNameRegEx;
    }

    public void setFullFileNameRegEx(String fullFileNameRegEx) {
        this.fullFileNameRegEx = fullFileNameRegEx;
    }

    public int getThreads() {
        return threads;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }
}
