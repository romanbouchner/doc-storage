package cz.direct.storage.extractor;

import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import cz.direct.storage.storage.Storage;
import cz.direct.storage.mongo.CollectionsService;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.tika.metadata.Metadata;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.InputStream;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExtractorRunnable implements Runnable {

    Logger logger = Logger.getLogger(ExtractorRunnable.class.getName());

    private ThreadPoolExecutor threadPoolExecutor;


    final private Storage storage;
    final private CollectionsService collectionsService;
    final private Extractor extractor;
    final private ExtractorConfigurationProperties extractorConf;

    private static final String METADATA_VERSION = "v1";

    public ExtractorRunnable(Storage storage, CollectionsService collectionsService, Extractor extractor, ExtractorConfigurationProperties extractorConf) {
        this.storage = storage;
        this.collectionsService = collectionsService;
        this.extractor = extractor;
        this.extractorConf = extractorConf;
    }

    @Override
    public void run() {
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(extractorConf.getThreads(), new BasicThreadFactory.Builder().namingPattern("WorkerExt-%d").build());

        Bson f1 = Filters.exists("metadata", false);
        //Bson f2 = Filters.exists("masterUuid", false);

        Bson filter;
        if (extractorConf.getFullFileNameRegEx() == null) {
            filter = f1;
        } else {
            Bson f3 = Filters.regex("fullFileName", extractorConf.getFullFileNameRegEx());
            filter = Filters.and(f1, f3);
        }

        final int MAX_QUEUE_SIZE = 100;

        try {
            while (true) {
                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }
                try {
                    MongoCursor<Document> mc = collectionsService.getStorageFilesCollection().find(filter).iterator();
                    try {
                        while (mc.hasNext()) {
                            if (Thread.interrupted()) {
                                throw new InterruptedException();
                            }
                            Document doc = mc.next();
                            String uuid = doc.getString("uuid");
                            String fullFileName = doc.getString("fullFileName");

                            threadPoolExecutor.submit(() -> {
                                try {
                                    try (InputStream is = storage.getFile(uuid)) {
                                        try {
                                            //extract metadata and store it to the database
                                            ExtractedContent extractedContent = extractor.extract(is);
                                            updateMetadata(uuid, extractedContent, fullFileName, null);
                                            //set flag we made it
                                            setMetadataFlag(uuid, METADATA_VERSION);
                                            logger.info("Processed " + fullFileName);
                                        } catch (ExtractException e) {
                                            updateMetadata(uuid, null, fullFileName, e.getMessage());
                                            //set flag we made it with error
                                            setMetadataFlag(uuid, "error");
                                            logger.log(Level.SEVERE, "Cannot extract metadata " + uuid + " " + fullFileName + "Error: " + e.getMessage(), e);
                                        }
                                    }
                                } catch (Exception e) {
                                    updateMetadata(uuid, null, fullFileName, e.getMessage());
                                    //set flag we made it with error
                                    setMetadataFlag(uuid, "error");
                                    logger.log(Level.SEVERE, "Cannot read/write file from/to storage " + uuid + " " + fullFileName + " Error: " + e.getMessage(), e);
                                }
                            });

                            //we do not want to fill a queue too much
                            while (threadPoolExecutor.getQueue().size() > MAX_QUEUE_SIZE) {
                                Thread.sleep(500);
                            }

                        }
                    } finally {
                        mc.close();
                    }

                } catch (InterruptedException e) {
                    throw e;
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Cannot start extractor main query Error: " + e.getMessage(), e);
                }

                //rest little bit before next query
                Thread.sleep(60000);

            }
        } catch (InterruptedException e) {
            logger.info("interrupted");
        }

        threadPoolExecutor.shutdownNow();
    }

    private void setMetadataFlag(String uuid, String flag) {
        collectionsService.getStorageFilesCollection().updateOne(Filters.eq("uuid", uuid), new Document("$set", new Document("metadata", flag)));
    }

    private static final UpdateOptions upsertOption = new UpdateOptions().upsert(true);

    private void updateMetadata(String uuid, ExtractedContent extractedContent, String fullFileName, String errorMessage) {
        collectionsService.getMetadataCollection().updateOne(Filters.eq("uuid", uuid), new Document("$set", metadataToDocument(uuid, extractedContent, fullFileName, new Date(), errorMessage)), upsertOption);
    }

    private static Document metadataToDocument(String uuid, ExtractedContent content, String fullFileName, Date extractDate, String errorMessage) {
        Document doc = new Document();
        doc.put("uuid", uuid);
        doc.put("fullFileName", fullFileName);
        doc.put("extractDate", extractDate);
        doc.put("version", METADATA_VERSION);
        if (errorMessage != null) doc.put("errorMessage", errorMessage);

        if (content != null) {
            doc.put("content", content.getText());
            Metadata metadata = content.getMetadata();
            Document tikaMetadata = new Document();
            String[] properties = metadata.names();
            for (String property : properties) {
                String value = metadata.get(property);
                tikaMetadata.put(property, value);
            }
            doc.put("tikaMetadata", tikaMetadata);
        }
        return doc;
    }

}
