package cz.direct.storage.extractor;

import cz.direct.storage.storage.Storage;
import cz.direct.storage.mongo.CollectionsService;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@Service
//TODO  pouzit mongoTemplate, resit timeout TIKA
public class ExtractorService {

    Logger logger = Logger.getLogger(ExtractorService.class.getName());

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ExtractorConfigurationProperties extractorConf;

    @Autowired
    private Storage storage;

    @Autowired
    private CollectionsService collectionsService;

    @Autowired
    private Extractor extractor;


    private ExecutorService extractorExecutorService;


    public void startExtractor() {
        if(!extractorConf.isEnabled()) {
            logger.info("Extractor is disabled");
            return;
        }

        extractorExecutorService = Executors.newSingleThreadExecutor(new BasicThreadFactory.Builder().namingPattern("MainExtr").build());

        extractorExecutorService.submit(new ExtractorRunnable(storage,  collectionsService, extractor,extractorConf));

    }

    @PreDestroy
    public void destroy() {
        logger.info("Destroy called");
        //set terminate signal
        if(extractorExecutorService != null) {
            extractorExecutorService.shutdownNow();
            //wait for finish
            try {
                System.out.println("Waiting for extractor thread");
                extractorExecutorService.awaitTermination(20, TimeUnit.SECONDS);
                System.out.println("OK");
            } catch (InterruptedException e) {
                System.out.println("Interrupted");
            }
        }
    }

}
