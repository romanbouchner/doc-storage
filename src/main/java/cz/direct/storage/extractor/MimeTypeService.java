package cz.direct.storage.extractor;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;

@Service
public class MimeTypeService {

    private MimetypesFileTypeMap mimetypesFileTypeMap;

    /**
     * Returns mime-type for given filename
     * @param fileName
     * @return
     */
    public String getMimeType(String fileName) {
        return mimetypesFileTypeMap.getContentType(fileName);
    }

    @PostConstruct
    private void init() throws IOException {
        try(InputStream is = new ClassPathResource("mime.types").getInputStream()) {
            mimetypesFileTypeMap = new MimetypesFileTypeMap(is);
        }
    }
}
