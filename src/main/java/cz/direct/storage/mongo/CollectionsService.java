package cz.direct.storage.mongo;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CollectionsService {
    @Autowired
    private MongoClient mongoClient;

    @Autowired
    private MongoConfig mongoConfig;

    public MongoCollection<Document> getStorageFilesCollection() {
        return mongoClient.getDatabase(mongoConfig.getDatabaseName()).getCollection("storageFiles");
    }

    public MongoCollection<Document> getMetadataCollection() {
        return mongoClient.getDatabase(mongoConfig.getDatabaseName()).getCollection("metadata");
    }

}
