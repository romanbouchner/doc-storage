package cz.direct.storage.mongo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("mongodb")
public class MongoConfigurationProperties {

    String uri;

    String databaseName;

    public String getUri() {
        return uri;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }
}
