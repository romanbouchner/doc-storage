package cz.direct.storage.preview;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class PreviewInfo {
    private final byte[] image;
    private final String mimeType;
    private final boolean cacheUsed;

    public PreviewInfo(byte[] image, String mimeType, boolean cacheUsed)  {
        this.image = image;
        this.mimeType = mimeType;
        this.cacheUsed = cacheUsed;
    }

    public byte[] getImage() {
        return image;
    }

    public int getFileLength() {
        return image.length;
    }

    public String getMimeType() {
        return mimeType;
    }

    public boolean isCacheUsed() {
        return cacheUsed;
    }
}
