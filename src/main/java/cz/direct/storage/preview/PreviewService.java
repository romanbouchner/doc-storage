package cz.direct.storage.preview;

import cz.direct.storage.storage.FileInfo;
import cz.direct.storage.storage.Storage;
import cz.direct.storage.preview.cache.PreviewCacheDocument;
import cz.direct.storage.preview.cache.PreviewCacheRepository;
import org.apache.commons.io.IOUtils;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class PreviewService {

    Logger logger = Logger.getLogger(PreviewService.class.getName());

    @Autowired
    Storage storage;

    @Autowired
    PreviewCacheRepository previewCacheRepository;

    @Autowired
    PreviewConfigurationProperties conf;

    private final String resultContentType = "image/png";
    private final String resultExtension = ".png";

    public PreviewInfo getThumbnail(String uuid, Integer width, Integer height) {
        if(!conf.isEnabled()) {
            logger.warning("Preview service is disabled");
            return getNAIcon();
        }

        if(width != null && (width < 10 || width > 500)) {
            logger.warning("Incorrect preview parameters width:" + width);
            return getNAIcon();
        }

        if(height != null && (height < 10 || height > 500)) {
            logger.warning("Incorrect preview parameters height:" + height);
            return getNAIcon();
        }

        //can we use cache?
        PreviewCacheDocument cachedDocument = previewCacheRepository.getCachedItem(uuid,width,height);
        if(cachedDocument != null) {
            logger.log(Level.FINE, "Cache is used for " + uuid + " " + width + " " + height);
            return new PreviewInfo(cachedDocument.getContent(),cachedDocument.getMimeType(),true);
        }

        File sourceTempFile = null;
        File destTempFile = null;
        try {
            FileInfo fileInfo = storage.getFileInfo(uuid);
            if(fileInfo != null) {
                String extension = getExtension(fileInfo.getFullFileName()).toLowerCase();
                if(extension.equals("jpg") || extension.equals("png") || extension.equals("pdf")) {
                    //file copy to temp
                    sourceTempFile = File.createTempFile("storageConvert","."+extension);
                    InputStream originalStream = storage.getFile(uuid);
                    FileOutputStream fos = new FileOutputStream(sourceTempFile);
                    IOUtils.copy(originalStream,fos);

                    //conversion via image magic
                    destTempFile = File.createTempFile("storageConvert",resultExtension);
                    ConvertCmd cmd = new ConvertCmd();
                    IMOperation op = new IMOperation();
                    op.addImage(sourceTempFile.getAbsolutePath()+"[0]"); // source file, just only the first page
                    op.alpha("remove");
                    op.background("white");
                    op.thumbnail(width,height);
                    op.addImage(destTempFile.getAbsolutePath()); // destination file file
                    cmd.run(op);

                    //we use memory, so we have to limit file size
                    if(destTempFile.length() > 20000000) {
                        throw new RuntimeException("The preview file is too big: " + destTempFile.length());
                    }
                    //ok, success
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    try(FileInputStream fis = new FileInputStream(destTempFile)) {
                        IOUtils.copy(fis,baos);
                    }

                    //store it to the cache
                    previewCacheRepository.putItem(new PreviewCacheDocument(uuid,width,height,baos.toByteArray(),resultContentType));

                    return new PreviewInfo(baos.toByteArray(),resultContentType,false);
                } else {
                    logger.log(Level.WARNING,"Cannot return preview for " + extension + " UUID:" + uuid);
                }
            }

        } catch (IOException e) {
            logger.log(Level.SEVERE, "Cannot generate a preview " + e.getMessage(),e);
        } catch (InterruptedException e) {
            logger.log(Level.SEVERE, "Cannot generate a preview " + e.getMessage(),e);
        } catch (IM4JavaException e) {
            logger.log(Level.SEVERE, "Cannot generate a preview " + e.getMessage(),e);
        } finally {
            try {
                if (sourceTempFile != null) sourceTempFile.delete();
            } finally {
                if (destTempFile != null) destTempFile.delete();
            }

        }

       return getNAIcon();
    }

    private PreviewInfo getNAIcon() {
        //in case of error we return NA icon
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try(InputStream fis = new ClassPathResource("na-icon.png").getInputStream()) {
                IOUtils.copy(fis,baos);
            }
            return new PreviewInfo(baos.toByteArray(),"image/png",false);
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Cannot return NA icon " + e.getMessage(),e);
            //give up
            return null;
        }
    }

    private static String getExtension(String fileName) {
        if(fileName.contains(".") && fileName.lastIndexOf(".")!= 0) {
            return fileName.substring(fileName.lastIndexOf(".")+1);
        }
        return "";
    }
}
