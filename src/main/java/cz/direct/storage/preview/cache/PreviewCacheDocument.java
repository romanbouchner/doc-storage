package cz.direct.storage.preview.cache;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "previewCache")
public class PreviewCacheDocument {

    @Indexed
    String uuid;

    Integer width;
    Integer height;
    byte[] content;
    String mimeType;

    public PreviewCacheDocument(String uuid, Integer width, Integer height, byte[] content, String mimeType) {
        this.uuid = uuid;
        this.width = width;
        this.height = height;
        this.content = content;
        this.mimeType = mimeType;
    }

    public String getUuid() {
        return uuid;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }

    public byte[] getContent() {
        return content;
    }

    public String getMimeType() {
        return mimeType;
    }
}
