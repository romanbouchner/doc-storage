package cz.direct.storage.preview.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class PreviewCacheRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    public PreviewCacheDocument getCachedItem(String uuid, Integer width, Integer height) {
        Criteria criteria = new Criteria().andOperator(
                Criteria.where("uuid").is(uuid),
                Criteria.where("width").is(width),
                Criteria.where("height").is(height)
        );
        return mongoTemplate.findOne(new Query(criteria),PreviewCacheDocument.class);
    }

    public void putItem(PreviewCacheDocument item) {
        mongoTemplate.insert(item);
    }
}
