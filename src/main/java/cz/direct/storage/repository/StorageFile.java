package cz.direct.storage.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "storageFiles")
public class StorageFile {

    @Indexed(unique = true)
    private String uuid;

    private String source;

    @Indexed
    private String fullFileName;

    private String masterUuid;

    @Indexed
    private String sha1;

    private long length;

    private List<String> actionRequest; //general request for a action

    public StorageFile(String uuid, String source, String fullFileName, String sha1, long length, String masterUuid, List<String> actionRequest) {
        this.uuid = uuid;
        this.source = source;
        this.fullFileName = fullFileName;
        this.masterUuid = masterUuid;
        this.sha1 = sha1;
        this.length = length;
        this.actionRequest = actionRequest;
    }


    public String getUuid() {
        return uuid;
    }

    public String getSource() {
        return source;
    }

    public String getFullFileName() {
        return fullFileName;
    }

    public String getMasterUuid() {
        return masterUuid;
    }

    public String getSha1() {
        return sha1;
    }

    public long getLength() {
        return length;
    }

    public String getStorageUuid() {
        if(getMasterUuid() != null) return masterUuid;
        return uuid;
    }

}
