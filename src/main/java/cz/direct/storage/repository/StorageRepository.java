package cz.direct.storage.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import com.mongodb.gridfs.GridFSDBFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.List;

@Repository
public class StorageRepository {

    private final GridFsTemplate gridFsTemplate;

    private final MongoTemplate mongoTemplate;

    private final GridFSBucket gridFSBucket;

    public StorageRepository(GridFsTemplate gridFsTemplate, MongoTemplate mongoTemplate) {
        this.gridFsTemplate = gridFsTemplate;
        this.mongoTemplate = mongoTemplate;
        gridFSBucket = GridFSBuckets.create(mongoTemplate.getDb());
    }

    @PostConstruct
    private void ensureIndexExist() {
        //ensures about the index in GridFS table
        mongoTemplate.getCollection("fs.files").createIndex(Indexes.ascending("metadata.uuid"),new IndexOptions().unique(true));
    }

    public void putFileInfo(StorageFile storageFile) {
        mongoTemplate.insert(storageFile);
    }

    public StorageFile getFileInfoByUUID(String uuid) {
        return mongoTemplate.findOne(new Query(Criteria.where("uuid").is(uuid)),StorageFile.class);
    }

    public List<StorageFile> getFileInfoBySHA1(String sha1) {
        return mongoTemplate.find(new Query(Criteria.where("sha1").is(sha1)),StorageFile.class);
    }

    public List<StorageFile> getFileInfoByFullFileName(String fullFileName) {
        return mongoTemplate.find(new Query(Criteria.where("fullFileName").is(fullFileName)),StorageFile.class);
    }

    public List<StorageFile> getFilesForAction(String action, int limit) {
        return mongoTemplate.find(new Query(Criteria.where("actionRequest").is(action)).limit(limit),StorageFile.class);
    }

    public void clearFileAction(String uuid, String action) {
        Update update = new Update();
        update.pull("actionRequest",action);
        mongoTemplate.updateFirst(new Query(Criteria.where("uuid").is(uuid)),update,StorageFile.class);
    }

    public void storeDataFile(InputStream is, String uuid) {
        DBObject metadata = new BasicDBObject();
        metadata.put("uuid",uuid);
        gridFsTemplate.store(is,"",metadata); //filename must not be null in a new mongo java driver
    }

    public void deleteDataFile(String uuid) {
        Query query = new Query().addCriteria(Criteria.where("metadata.uuid").is(uuid));
        gridFsTemplate.delete(query);
    }

    public InputStream getDataFile(String uuid) {
        Query query = new Query().addCriteria(Criteria.where("metadata.uuid").is(uuid));
        GridFSFile gridFile = gridFsTemplate.findOne(query);
        if(gridFile == null) return null;
        return gridFSBucket.openDownloadStream(gridFile.getObjectId());
    }
}
