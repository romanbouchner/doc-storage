package cz.direct.storage.storage;

import java.util.Objects;

public class FileInfo {
    private final String uuid;
    private final long length;
    private final String masterUuid;
    private final String source;
    private final String fullFileName;

    public FileInfo(String uuid, long length, String masterUuid, String source, String fullFileName) {
        this.uuid = uuid;
        this.length = length;
        this.masterUuid = masterUuid;
        this.source = source;
        this.fullFileName = fullFileName;
    }

    public String getUuid() {
        return uuid;
    }

    public long getLength() {
        return length;
    }

    public String getMasterUuid() {
        return masterUuid;
    }

    public String getSource() {
        return source;
    }

    public String getFullFileName() {
        return fullFileName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileInfo fileInfo = (FileInfo) o;
        return length == fileInfo.length &&
                Objects.equals(uuid, fileInfo.uuid) &&
                Objects.equals(masterUuid, fileInfo.masterUuid) &&
                Objects.equals(source, fileInfo.source) &&
                Objects.equals(fullFileName, fileInfo.fullFileName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(uuid, length, masterUuid, source, fullFileName);
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "uuid='" + uuid + '\'' +
                ", length=" + length +
                ", masterUuid='" + masterUuid + '\'' +
                ", source='" + source + '\'' +
                ", fullFileName='" + fullFileName + '\'' +
                '}';
    }
}
