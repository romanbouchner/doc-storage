package cz.direct.storage.storage;

import com.mongodb.client.model.Filters;
import cz.direct.storage.extractor.MimeTypeService;
import cz.direct.storage.mongo.CollectionsService;
import cz.direct.storage.repository.StorageFile;
import cz.direct.storage.repository.StorageRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.activation.MimetypesFileTypeMap;
import java.io.*;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Service for storing and retrieving files based on UUID.
 */
@Service
public class Storage {

    Logger logger = Logger.getLogger(Storage.class.getName());

    @Autowired
    StorageRepository storageRepository;

    @Autowired
    CollectionsService collectionsService;

    /**
     * Stores content into the storage
     * @param is file content
     * @param uuid unique ID. ID is generated by client application. UUID must be unique
     * @param source optional - source of file. Defines client appliacation.
     * @param fullFilePath optional - file path. Does not necessary be unique.
     *
     * @return true if file is duplication
     * @throws IOException
     */
    public boolean storeFile(InputStream is, String uuid, String source, String fullFilePath) throws IOException{
        return storeFile(is,uuid,source,fullFilePath,null);
    }

    /**
     * Stores content into the storage
     * @param is file content
     * @param uuid unique ID. ID is generated by client application. UUID must be unique
     * @param source optional - source of file. Defines client appliacation.
     * @param fullFilePath optional - file path. Does not necessary be unique.
     * @param actionsRequested - optional - the requested actions for further processing.
     *
     * @return true if file is duplication
     * @throws IOException
     */
    public boolean storeFile(InputStream is, String uuid, String source, String fullFilePath, List<String> actionsRequested) throws IOException{
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA1");
        } catch (NoSuchAlgorithmException e) {
            //should never happen
            throw new RuntimeException(e);
        }

        //we use temporary file to count SHA1
        File tempFile = File.createTempFile("storage_temp",".bin");
        try {
            String sha1;
            try(OutputStream os = new FileOutputStream(tempFile)) {
                try(DigestOutputStream dos = new DigestOutputStream(os,md)) {
                    IOUtils.copy(is,dos);
                    sha1 = DigestUtils.sha1Hex(dos.getMessageDigest().digest());
                }
            }

            //check if file already exists in the database
            //it is not atomic - if two threads with the same file go there, the file duplication may occur, but we do not care - it is not a big issue
            List<StorageFile> files = storageRepository.getFileInfoBySHA1(sha1);
            //there should not be too many files in the list - typically zero or one
            for(StorageFile file: files) {
                logger.info("Duplication found. UUID: " + file.getUuid());

                if(file.getMasterUuid() != null) {
                    //this is not record with original data
                    if(file == files.get(files.size()-1)) {
                        //it must not happen if it is a last record - at least one record without masterUuid should be available
                        throw new RuntimeException("Unexpected error with database consistency");
                    }
                    logger.info("Not original, skipping.");
                    continue;
                }
                //the file probably exist in the database, but we must be 100% sure - compare it byte by byte
                if(file.getLength() == tempFile.length()) {
                try(InputStream gridStream = storageRepository.getDataFile(file.getUuid())) {
                    try(InputStream tempStream = new FileInputStream(tempFile)) {
                        if(IOUtils.contentEquals(gridStream,tempStream)) {
                            //ok we do not need to store the file, store just file info
                            StorageFile storageFile = new StorageFile(uuid,source,fullFilePath,sha1,tempFile.length(),file.getUuid(),actionsRequested);
                            storageRepository.putFileInfo(storageFile);
                            return true;
                        }
                    }
                }
                }
            }

            //the file does not exist yet
            try(InputStream tempStream = new FileInputStream(tempFile)) {
                storageRepository.storeDataFile(tempStream,uuid);
                //register new file if everything is ok (we are not in transaction)
                StorageFile storageFile = new StorageFile(uuid,source,fullFilePath,sha1,tempFile.length(),null,actionsRequested);
                storageRepository.putFileInfo(storageFile);
            }

            return false;

        } finally {
            tempFile.delete();
        }
    }

    /**
     * Returns file content
     * @param uuid
     * @return
     */
    public InputStream getFile(String uuid) {
        StorageFile storageFile = storageRepository.getFileInfoByUUID(uuid);
        if(storageFile == null) {
            //file does not exist
            return null;
        }
        return storageRepository.getDataFile(storageFile.getStorageUuid());
    }

    /**
     * Returns file metadata
     * @param uuid
     * @return
     */
    public FileInfo getFileInfo(String uuid) {
        StorageFile storageFile = storageRepository.getFileInfoByUUID(uuid);
        if(storageFile == null) return null;
        return toFileInfo(storageFile);
    }

    /**
     * Returns file metadata based on fullFilename. FullFileName is not unique thus several objects can be returned.
     * @param fullFileName
     * @return
     */
    public List<FileInfo> getFileInfoByFullFileName(String fullFileName) {
        return storageRepository.getFileInfoByFullFileName(fullFileName).stream().map(storageFile -> toFileInfo(storageFile)).collect(Collectors.toList());
    }

    private static FileInfo toFileInfo(StorageFile storageFile) {
        return new FileInfo(storageFile.getUuid(),storageFile.getLength(),storageFile.getMasterUuid(), storageFile.getSource(), storageFile.getFullFileName());
    }
}
