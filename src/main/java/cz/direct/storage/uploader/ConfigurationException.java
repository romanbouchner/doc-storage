package cz.direct.storage.uploader;

public class ConfigurationException extends Exception {
    public ConfigurationException(String msg) {
        super(msg);
    }
}
