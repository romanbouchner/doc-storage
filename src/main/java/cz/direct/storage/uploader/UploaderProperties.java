package cz.direct.storage.uploader;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties("uploader")
public class UploaderProperties {

    private boolean enabled = false;
    private boolean dryRun = true;
    private boolean uuidPumpEnabled = true;

    private List<ImportConfiguration> imports = new ArrayList<>();

    public List<ImportConfiguration> getImports() {
        return imports;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isDryRun() {
        return dryRun;
    }

    public void setDryRun(boolean dryRun) {
        this.dryRun = dryRun;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isUuidPumpEnabled() {
        return uuidPumpEnabled;
    }

    public void setUuidPumpEnabled(boolean uuidPumpEnabled) {
        this.uuidPumpEnabled = uuidPumpEnabled;
    }

    public static class ImportConfiguration {
        private String name;
        private String importBaseFolder;
        private String excludeImportRegEx;
        private String destPrefixFolder;
        private String source;
        boolean enabled = true;
        boolean recursive = true;
        private Duration waitingTime = Duration.ofMinutes(10);

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImportBaseFolder() {
            return importBaseFolder;
        }

        public void setImportBaseFolder(String importBaseFolder) {
            this.importBaseFolder = importBaseFolder;
        }

        public String getExcludeImportRegEx() {
            return excludeImportRegEx;
        }

        public void setExcludeImportRegEx(String excludeImportRegEx) {
            this.excludeImportRegEx = excludeImportRegEx;
        }

        public String getDestPrefixFolder() {
            return destPrefixFolder;
        }

        public void setDestPrefixFolder(String destPrefixFolder) {
            this.destPrefixFolder = destPrefixFolder;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public Duration getWaitingTime() {
            return waitingTime;
        }

        public void setWaitingTime(Duration waitingTime) {
            this.waitingTime = waitingTime;
        }

        public boolean isRecursive() {
            return recursive;
        }

        public void setRecursive(boolean recursive) {
            this.recursive = recursive;
        }
    }

}
