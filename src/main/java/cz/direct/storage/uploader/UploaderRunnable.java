package cz.direct.storage.uploader;

import cz.direct.storage.storage.FileInfo;
import cz.direct.storage.storage.Storage;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UploaderRunnable implements Runnable {

	private static Logger logger = Logger.getLogger(UploaderRunnable.class.getName());

	private final UploaderProperties.ImportConfiguration configuration;
	private final boolean dryRun;
	private final String name;
	private final Storage storage;

	private int filesNum = 0;
	private int foldersNum = 0;
	private int ignoredNum = 0;
	private int duplicatedNum = 0;
	private int fileLengthWarningNum = 0;

	public UploaderRunnable(Storage storage, UploaderProperties.ImportConfiguration configuration, int id, boolean dryRun) {
		this.storage = storage;
		this.configuration = configuration;
		this.name = configuration.getName() != null ? configuration.getName() : "Uploader" + id;
		this.dryRun = dryRun;
	}

	public void testConfiguration() throws ConfigurationException {
		if (configuration.getImportBaseFolder() == null) {
			throw new ConfigurationException("Missing importBaseFolder configuration.");
		}

		if (configuration.getDestPrefixFolder() == null) {
			throw new ConfigurationException("Missing destPrefixFolder configuration.");
		}

		if (configuration.getSource() == null) {
			throw new ConfigurationException("Missing source configuration.");
		}
	}

	public String getName() {
		return name;
	}

	private void logInfo(String msg) {
		logger.info("[" + getName() + "] " + (dryRun ? "DRY RUN " : "") + msg);
	}

	@Override
	public void run() {
		logInfo("Starting uploader with folder: " + configuration.getImportBaseFolder() + " " + (configuration.isRecursive() ? "including sub-folders" : "without sub-folders"));

		if (!configuration.isEnabled()) {
			logInfo("Configuration is disabled, exiting..");
			return;
		}

		if (configuration.getExcludeImportRegEx() != null) {
			logInfo("excludeImportRegEx is : " + configuration.getExcludeImportRegEx());
		}

		if (dryRun) {
			logInfo("DRY RUN IS ENABLED");
		}


		while (true) {
			try {
				filesNum = 0;
				foldersNum = 0;
				ignoredNum = 0;
				duplicatedNum = 0;
				fileLengthWarningNum = 0;
				processFolder("", configuration.isRecursive());
				logInfo("Processed: files:" + filesNum + " folders:" + foldersNum + " ignored:" + ignoredNum + " duplicated:" + duplicatedNum + " warnings: " + fileLengthWarningNum);

				if (fileLengthWarningNum > 0) {
					logger.log(Level.WARNING, getName() + ": There are " + fileLengthWarningNum + " files with different file size. Probably the files were uploaded uncompleted.");

				}
				Thread.sleep(configuration.getWaitingTime().toMillis());
			} catch (InterruptedException e) {
				logInfo("Interrupted");
				break; //exit from import loop
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Unexpected error, current loader is disabled for one hour: " + e.getMessage(), e);
				try {
					TimeUnit.HOURS.sleep(1);
				} catch (InterruptedException e1) {
					logInfo("Interrupted");
					break; //exit from import loop
				}
			}
		}

	}

	private void processFolder(String relative, boolean recursive) throws InterruptedException {
		File folder = new File(configuration.getImportBaseFolder(), relative);

		File[] list = folder.listFiles();
		for (File file : list) {
			if (Thread.interrupted()) {
				throw new InterruptedException();
			}

			if (file.isFile()) {
				if (configuration.getExcludeImportRegEx() != null && file.getAbsolutePath().matches(configuration.getExcludeImportRegEx())) {
					logger.fine("Excluding file: " + file.getAbsolutePath());
					continue;
				}
				filesNum++;
				processFile(addPath(relative, file.getName()));
			}
			if (recursive && file.isDirectory()) {
				if (configuration.getExcludeImportRegEx() != null && file.getAbsolutePath().matches(configuration.getExcludeImportRegEx())) {
					logInfo("Excluding folder: " + file.getAbsolutePath());
					continue;
				}
				logInfo(filesNum + "/" + foldersNum + "/" + ignoredNum + "/" + duplicatedNum + " Processing folder: " + file.getAbsolutePath());
				foldersNum++;
				processFolder(addPath(relative, file.getName()), recursive);
			}
		}
	}

	private static String addPath(String base, String path) {
		if (base.isEmpty()) {
			return path;
		} else {
			return base + "/" + path;
		}
	}

	private void processFile(String fileName) {
		File sourceFile = new File(configuration.getImportBaseFolder(), fileName);
		String storeFileName = new File(configuration.getDestPrefixFolder(), fileName).getAbsolutePath();

		//do we already run import with the file?
		if (testIfFileAlreadyExist(storeFileName, sourceFile.length())) {
			//ignore it
			ignoredNum++;
			return;
		}

		String uuid = UUID.randomUUID().toString();

		logInfo(sourceFile.getAbsolutePath() + " -> " + storeFileName + " " + uuid);
		if (!dryRun) {
			try {
				try (FileInputStream fis = new FileInputStream(sourceFile)) {
					boolean duplicate = storage.storeFile(fis, uuid, configuration.getSource(), storeFileName, Arrays.asList(UploaderConst.UUID_REGISTRATION_REQUEST));
					//get info
					if (duplicate) {
						duplicatedNum++;
					}
				}
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Cannot import file " + fileName + " " + e.getMessage(), e);
			}
		}
	}

	private boolean testIfFileAlreadyExist(String storeFileName, long fileLength) {
		List<FileInfo> list = storage.getFileInfoByFullFileName(storeFileName);
		if (list.size() == 0) {
			return false;
		}

		for (FileInfo fileInfo : list) {
			if (fileInfo.getLength() != fileLength) {
				logger.log(Level.WARNING, "The file has a different length " + fileInfo.getLength() + "(db) vs " + fileLength + "(real). UUID: " + fileInfo.getUuid() + " " + fileInfo.getFullFileName());
				fileLengthWarningNum++;
			}
		}

		return true;
	}
}
