package cz.direct.storage.uploader;

import cz.direct.storage.storage.Storage;
import cz.direct.storage.repository.StorageRepository;
import cz.direct.storage.uploader.uuidpump.UUIDRepository;
import cz.direct.storage.uploader.uuidpump.UuidPumpRunnable;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class UploaderService {

    private Logger logger = Logger.getLogger(UploaderService.class.getName());

    @Autowired
    private UploaderProperties uploaderProperties;

    @Autowired
    private Storage storage;

    @Autowired
    StorageRepository storageRepository;

    @Autowired
    UUIDRepository uuidRepository;

    private ExecutorService threadPoolExecutorUploaders;
    private ExecutorService threadPoolExecutorUUIDPump;

    public void start() {
        if (!uploaderProperties.isEnabled()) {
            logger.info("Uploader is disabled");
            return;
        }

        threadPoolExecutorUploaders =  Executors.newCachedThreadPool(new BasicThreadFactory.Builder().namingPattern("Uploader-%d").build());

        threadPoolExecutorUUIDPump = Executors.newSingleThreadExecutor(new BasicThreadFactory.Builder().namingPattern("UUIDPump-%d").build());

        List<UploaderProperties.ImportConfiguration> configurations = uploaderProperties.getImports();

        //test configuration
        boolean configurationOk = true;
        int id = 0;
        List<UploaderRunnable> uploaders = new ArrayList<>();
        for (UploaderProperties.ImportConfiguration conf : configurations) {
            UploaderRunnable uploaderRunnable = new UploaderRunnable(storage, conf, id++, uploaderProperties.isDryRun());
            uploaders.add(uploaderRunnable);
            try {
                uploaderRunnable.testConfiguration();
            } catch (ConfigurationException e) {
                logger.log(Level.SEVERE, "Invalid configuration: " + e.getMessage());
                configurationOk = false;
            }
        }

        if (!configurationOk) {
            logger.log(Level.SEVERE, "Uploader is disabled now");
            return;
        }

        //start uploaders
        for (UploaderRunnable uploader : uploaders) {
            threadPoolExecutorUploaders.submit(uploader);
            logger.log(Level.INFO, uploader.getName() + " Started");
        }

        //start uuidpump
        if (uploaderProperties.isUuidPumpEnabled() && !uploaderProperties.isDryRun()) {
            threadPoolExecutorUUIDPump.submit(new UuidPumpRunnable(uuidRepository, storageRepository));
            logger.log(Level.INFO, "UuidPump Started");
        } else {
            logger.log(Level.INFO, "UuidPump is disabled.");
        }
    }

    @PreDestroy
    public void destroy() {
        //logger may not working in PreDestroy
        if (uploaderProperties.isEnabled()) {
            System.out.println("Asking uploader threads to finish");
            threadPoolExecutorUUIDPump.shutdownNow();
            threadPoolExecutorUploaders.shutdownNow();

            //wait for all threads
            System.out.println("Waiting for all uploader threads to finish");

            try {
                threadPoolExecutorUUIDPump.awaitTermination(20, TimeUnit.SECONDS);
                threadPoolExecutorUploaders.awaitTermination(20, TimeUnit.SECONDS);
                System.out.println("OK");
            } catch (InterruptedException e) {
                logger.info("Interrupted");
            }


            System.out.println("All threads finished.");
            logger.info("test XXXXXXXXXXXXXXXXXXXXXXXXx");
        }
    }

}
