package cz.direct.storage.uploader.uuidpump;

import cz.direct.storage.repository.StorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
/**
 * Temporary repository for UUID pump (will be removed later, there should not be PGSQL dependency)
 */
public class UUIDRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     *
     * @param uuid
     * @param fullFileName
     * @return returns true, if uuid was inserted, false if UUID already exists in the table.
     */
    public boolean storeUUID(UUID uuid, String fullFileName) {
        try {
            jdbcTemplate.update("INSERT INTO adinsure.comm_document_migration (uuid,fullfilename) VALUES (?,?)", uuid, fullFileName);
            return true;
        } catch (DuplicateKeyException e) {
            return false;
        }
    }

}
