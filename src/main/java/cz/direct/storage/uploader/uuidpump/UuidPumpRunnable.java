package cz.direct.storage.uploader.uuidpump;

import cz.direct.storage.repository.StorageFile;
import cz.direct.storage.repository.StorageRepository;
import cz.direct.storage.uploader.UploaderConst;

import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Pushes information about UUID into PostgreSQL database for further processing
 *
 * How to ask to process all UUIDs again:
 * db.storageFiles.updateMany({},{$push:{actionRequest:"uuidrequest"}})

 */
public class UuidPumpRunnable implements Runnable {

    private Logger logger = Logger.getLogger(UuidPumpRunnable.class.getName());

    private UUIDRepository uuidRepository;
    private StorageRepository storageRepository;

    public UuidPumpRunnable(UUIDRepository uuidRepository, StorageRepository storageRepository) {
        this.uuidRepository = uuidRepository;
        this.storageRepository = storageRepository;
    }

    @Override
    public void run() {

            while (true) {
                try {
                    List<StorageFile> list = storageRepository.getFilesForAction(UploaderConst.UUID_REGISTRATION_REQUEST,100000);
                    if (list.size() > 0) logger.info("Updating UUID status. Number to be updated: " + list.size());

                    for (StorageFile storageFile : list) {
                        if (Thread.interrupted()) {
                            throw new InterruptedException();
                        }

                        boolean inserted = uuidRepository.storeUUID(UUID.fromString(storageFile.getUuid()), storageFile.getFullFileName());
                        if(inserted) {
                            logger.log(Level.INFO,"UUID " + storageFile.getUuid()+" was inserted");
                        } else {
                            logger.log(Level.INFO,"UUID " + storageFile.getUuid()+" is already in the database");
                        }
                        //set flag
                        storageRepository.clearFileAction(storageFile.getUuid(), UploaderConst.UUID_REGISTRATION_REQUEST);
                    }

                    if (list.size() == 0) {
                        //if we have finished already, we can rest for a while
                        Thread.sleep(10000);
                    } else {
                        logger.info("Updating UUID status, processed: " + list.size());
                        Thread.sleep(1);
                    }
                } catch (InterruptedException e) {
                    logger.info("Interrupted");
                    return;
                } catch (Exception e) {
                    logger.log(Level.SEVERE,"UUID pump general error: " + e.getMessage(),e);
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e1) {
                        logger.info("Interrupted");
                        return;
                    }
                    //try it next time
                    continue;
                }
            }


    }
}
