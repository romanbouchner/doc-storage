package cz.direct.storage;

import cz.direct.storage.extractor.ExtractException;
import cz.direct.storage.extractor.ExtractedContent;
import cz.direct.storage.extractor.Extractor;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ExtractorTest {

    private Extractor extractor = new Extractor();

    @Test
    public void extractPNG() throws IOException, ExtractException {
        //File simplePDF = new ClassPathResource("simple.pdf").getFile();
        File simplePDF = new ClassPathResource("test-data/image.png").getFile();
        try(FileInputStream fis = new FileInputStream(simplePDF)) {
            ExtractedContent ec = extractor.extract(fis);
            Assert.assertEquals("668",ec.getMetadata().get("tiff:ImageWidth"));
            Assert.assertEquals("image/png",ec.getMetadata().get("Content-Type"));
        }
    }

    @Test
    public void extractPDF() throws IOException, ExtractException {
        File simplePDF = new ClassPathResource("test-data/simple.pdf").getFile();
        try(FileInputStream fis = new FileInputStream(simplePDF)) {
            ExtractedContent ec = extractor.extract(fis);
            Assert.assertEquals("Simple PDF ěščřžýáíé=",ec.getText().trim());
            Assert.assertEquals("application/pdf",ec.getMetadata().get("Content-Type"));
        }
    }
}
