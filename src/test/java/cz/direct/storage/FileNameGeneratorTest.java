package cz.direct.storage;

import cz.direct.storage.communication.FileNameGenerator;
import org.junit.Assert;
import org.junit.Test;

public class FileNameGeneratorTest {

    @Test
    public void testEmptyAndNull() {
        FileNameGenerator fn = new FileNameGenerator();
        Assert.assertEquals("NA",fn.generateName(null,20));
        Assert.assertEquals("1-NA",fn.generateName("  ",20));
        Assert.assertEquals("2-NA",fn.generateName(null,20));
    }

    @Test
    public void testCharacters() {
        FileNameGenerator fn = new FileNameGenerator();
        Assert.assertEquals("čřž_šščaa",fn.generateName("čřž ššč/aa",20));
        Assert.assertEquals("1-čřž_šščaa",fn.generateName("čřž ššč/aa",20));
        Assert.assertEquals("čř",fn.generateName("čřž ššč/aa",2));
    }
}
