package cz.direct.storage;

import cz.direct.storage.preview.PreviewInfo;
import cz.direct.storage.preview.PreviewService;
import cz.direct.storage.storage.Storage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataMongoTest
@Import(TestConfiguration.class)
public class PreviewTest {

    @Autowired
    PreviewService previewService;

    @Autowired
    Storage storage;

    @Test
    public void testPreviewCache() throws IOException {
        File simplePDF = new ClassPathResource("test-data/simple.pdf").getFile();
        String uuid = UUID.randomUUID().toString();
        try(FileInputStream fis = new FileInputStream(simplePDF)) {
            storage.storeFile(fis,uuid,"test","file.pdf");
        }

        PreviewInfo pi = previewService.getThumbnail(uuid,100,null);
        Assert.assertEquals("image/png",pi.getMimeType());
        Assert.assertEquals(false,pi.isCacheUsed());

        PreviewInfo pi2 = previewService.getThumbnail(uuid,100,null);
        Assert.assertEquals("image/png",pi2.getMimeType());
        Assert.assertEquals(true,pi2.isCacheUsed());
        Assert.assertEquals(pi.getFileLength(),pi2.getFileLength());

        PreviewInfo pi3 = previewService.getThumbnail(uuid,100,100);
        Assert.assertEquals("image/png",pi3.getMimeType());
        Assert.assertEquals(false,pi3.isCacheUsed());

        PreviewInfo pi4 = previewService.getThumbnail(uuid,100,100);
        Assert.assertEquals("image/png",pi4.getMimeType());
        Assert.assertEquals(true,pi4.isCacheUsed());
        Assert.assertEquals(pi3.getFileLength(),pi4.getFileLength());

        PreviewInfo pi5 = previewService.getThumbnail(uuid,200,200);
        Assert.assertNotEquals(pi5.getFileLength(),pi4.getFileLength());
        Assert.assertEquals(false,pi5.isCacheUsed());
    }
}
