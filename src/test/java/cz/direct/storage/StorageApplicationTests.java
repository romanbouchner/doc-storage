package cz.direct.storage;

import cz.direct.storage.extractor.MimeTypeService;
import cz.direct.storage.repository.StorageFile;
import cz.direct.storage.repository.StorageRepository;
import cz.direct.storage.storage.FileInfo;
import cz.direct.storage.storage.Storage;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureJdbc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataMongoTest
@Import(TestConfiguration.class)
public class StorageApplicationTests {

	@Autowired
    Storage storage;

	@Autowired
	StorageRepository storageRepository;

	@Autowired
	MimeTypeService mimeTypeService;

	@Test
	public void testUniqueUUID() throws IOException {
		final int FILE_SIZE = 10000000;
		byte[] testFile = createRandomArray(FILE_SIZE);
		String uuid = UUID.randomUUID().toString();
		String source = "source";
		storage.storeFile(new ByteArrayInputStream(testFile),uuid,source,null);

		//the second call with the same uuid should fail
		try {
			storage.storeFile(new ByteArrayInputStream(testFile), uuid, source, null);
			Assert.fail("No duplication detection");
		} catch (org.springframework.dao.DuplicateKeyException e) {
			//ok
		}
	}

	@Test
	public void testZeroFile() throws IOException {
		byte[] testFile =new byte[0];
		String uuid = UUID.randomUUID().toString();
		String source = "source";
		storage.storeFile(new ByteArrayInputStream(testFile),uuid,source,null);
		Assert.assertEquals(0,storage.getFileInfo(uuid).getLength());
	}

	@Test
	public void testDuplicatedFile() throws IOException {
		final int FILE_SIZE = 4532567;
		byte[] testFile = createRandomArray(FILE_SIZE);

		//master file
		String uuid = createAndTestFile(testFile,null);

		//duplicated file should have reference to master
		createAndTestFile(testFile,uuid);
	}

	@Test
	public void testMetadata() throws IOException {
		Random r = new Random();
		final int FILE_SIZE = 100;
		byte[] testFile = createRandomArray(FILE_SIZE);

		//master file
		String uuid = UUID.randomUUID().toString();
		String source = "testAPP" + r.nextLong();
		String filePath = "/home/test" + r.nextLong() + ".txt";
		storage.storeFile(new ByteArrayInputStream(testFile),uuid,source,filePath);
		FileInfo fi = storage.getFileInfo(uuid);
		Assert.assertEquals(fi.getLength(),FILE_SIZE);
		Assert.assertEquals(fi.getUuid(),uuid);
		Assert.assertEquals(fi.getMasterUuid(),null);
		Assert.assertEquals(fi.getSource(),source);
		Assert.assertEquals(fi.getFullFileName(),filePath);

		List<FileInfo> info = storage.getFileInfoByFullFileName(filePath);
		Assert.assertEquals(1,info.size());
		Assert.assertEquals(fi,info.get(0));
	}

	@Test
	public void testWriteRead() throws IOException {
		final int FILE_SIZE = 17876567;
		byte[] testFile = createRandomArray(FILE_SIZE);

		String uuid = UUID.randomUUID().toString();
		storage.storeFile(new ByteArrayInputStream(testFile),uuid,null,null);

		try (InputStream storedFileStream = storage.getFile(uuid)) {
			InputStream testFIleStream = new ByteArrayInputStream(testFile);
			Assert.assertEquals(true,IOUtils.contentEquals(storedFileStream,testFIleStream));
		}

	}

	@Test
	public void testMultipleDuplicate() throws IOException {
		final int FILE_SIZE = 545687;
		byte[] testFile = createRandomArray(FILE_SIZE);

		//master file
		String masterUuid = createAndTestFile(testFile,null);

		//duplicated file should have reference to master
		createAndTestFile(testFile,masterUuid);
		createAndTestFile(testFile,masterUuid);
		createAndTestFile(testFile,masterUuid);

	}

	@Test
	public void testActionRequests() throws IOException {
		String uuid = UUID.randomUUID().toString();
		final String action = UUID.randomUUID().toString();
		storage.storeFile(new ByteArrayInputStream(createRandomArray(255)),uuid,"source","/mujtest", Arrays.asList(action));
		List<StorageFile> list = storageRepository.getFilesForAction(action,10);
		Assert.assertEquals(1,list.size());
		storageRepository.clearFileAction(uuid,action);
		list = storageRepository.getFilesForAction(action,10);
		Assert.assertEquals(0,list.size());
	}

	private String createAndTestFile(byte[] content, String checkMasterUUID) throws IOException {
		String uuid = UUID.randomUUID().toString();
		storage.storeFile(new ByteArrayInputStream(content),uuid,"source",null);
		FileInfo fi = storage.getFileInfo(uuid);
		Assert.assertEquals(fi.getLength(),content.length);
		Assert.assertEquals(fi.getUuid(),uuid);
		Assert.assertEquals(fi.getMasterUuid(),checkMasterUUID);
		return uuid;
	}

	private static byte[] createRandomArray(int length) {
		Random r = new Random();
		byte[] buff = new byte[length];
		r.nextBytes(buff);
		return buff;
	}

	@Test
	public void mimeTypeTest() {
		Assert.assertEquals("application/vnd.ms-excel",mimeTypeService.getMimeType("test.xls"));
		Assert.assertEquals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",mimeTypeService.getMimeType("test.xlsx"));
	}
}
