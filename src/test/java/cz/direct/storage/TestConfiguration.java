package cz.direct.storage;

import cz.direct.storage.copy.CopyService;
import cz.direct.storage.copy.CopyServiceConfigurationProperties;
import cz.direct.storage.extractor.Extractor;
import cz.direct.storage.extractor.ExtractorConfigurationProperties;
import cz.direct.storage.extractor.ExtractorService;
import cz.direct.storage.extractor.MimeTypeService;
import cz.direct.storage.mongo.CollectionsService;
import cz.direct.storage.mongo.MongoConfig;
import cz.direct.storage.mongo.MongoConfigurationProperties;
import cz.direct.storage.preview.PreviewConfigurationProperties;
import cz.direct.storage.preview.PreviewService;
import cz.direct.storage.preview.cache.PreviewCacheRepository;
import cz.direct.storage.repository.StorageRepository;
import cz.direct.storage.storage.Storage;
import cz.direct.storage.uploader.UploaderProperties;
import cz.direct.storage.uploader.UploaderService;
import cz.direct.storage.uploader.uuidpump.UUIDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class TestConfiguration {

    @Bean
    Storage storage() {
        return new Storage();
    }

    @Bean
    StorageRepository storageRepository(GridFsTemplate gridFsTemplate,MongoTemplate mongoTemplate) {
        return new StorageRepository(gridFsTemplate,mongoTemplate);
    }

    @Bean
    ExtractorService extractorService() {return new ExtractorService();}


    @Bean
    PreviewConfigurationProperties previewConfigurationProperties() {return new PreviewConfigurationProperties();}

    @Bean
    MongoConfigurationProperties mongoConfigurationProperties() {return new MongoConfigurationProperties();}

    @Bean
    ExtractorConfigurationProperties extractorConfigurationProperties() {return new ExtractorConfigurationProperties();}

    @Bean
    UploaderProperties uploaderProperties() {return new UploaderProperties();}

    @Bean
    UploaderService uploaderService() {return new UploaderService();}

    @Bean
    CollectionsService collectionsService() {return new CollectionsService();}

    @Bean
    MongoConfig mongoConfig() {return new MongoConfig();}

    @Bean
    PreviewService previewService() {return new PreviewService();}

    @Bean
    PreviewCacheRepository reviewCacheRepository() {return new PreviewCacheRepository();}

    @Bean
    Extractor extractor() {
        return new Extractor();
    }

    @Bean
    UUIDRepository uuidRepository() {
        return new UUIDRepository();
    }

    @Bean
    MimeTypeService mimeTypeService() {
        return new MimeTypeService();
    }

    @Bean
    JdbcTemplate jdbcTemplate() {
        EmbeddedDatabase db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.HSQL)
                //.addScript("createPersonTable.sql")
                .build();
        return new JdbcTemplate(db);
    }

    @Bean
    CopyService copyService() {
        return new CopyService();
    }

    @Bean
    CopyServiceConfigurationProperties copyServiceConfigurationProperties() {
        return new CopyServiceConfigurationProperties();
    }
}
